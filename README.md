# Install flux and configurer GKE using weave flux

## Purpose
Configure the Google Cloud Kubernetes cluster the GitOps way thanks to Weave flux.  
After the configuration and provisioning of a GKE cluster, this pipeline installs Weave Flux on the cluster and then Weave Flux installs the desired namespaces and security policies defined here in this repository. 

## Usage
### IAM configuration
#### GCP IAM configuration
On GCP, open `IAM & admin` > `Service accounts` from the sidebar and click on `CREATE SERVICE ACCOUNT`. 
- Step1: Service account details
  - Service account Name: **gitlab-ci**
  - Service account ID: **gitlab-ci**@{project}.iam.gservice.com
  - Service account description: **gitlab continuous integration**
  - Click on `CREATE`. 
- Step2: Grant the service account access to the project
  - Select a Role: **Kubernetes Engine** > **Kubernetes Engine Admin**
  - Click on `CONTINUE`
- Step3: Grant users access to this service account
  - Click on `CREATE KEY`
  - Choose `JSON` (the key is downloaded as a json file)
- Click on `DONE`

#### GitLab CI Environment variables
On InnerSource, open `Settings` > `CICD` and expand the `Environment variables` section. Add the following variables. 
- `GCLOUD_PROJECT_ID`: {YOUR_GCP_PROJECT_ID}
- `GCLOUD_SERVICE_KEY`: {CONTENT_OF_PREVIOUS_GCP_DOWNLOADED_JSON_FILE}
- `GLCOUD_COMPUTE_ZONE`: {YOUR_GCP_COMPUTE_ZONE}
- `GLCOUD_CLUSTER_NAME`: {YOUR_COMPUTE_ZONE}
- `GITLAB_ACCESS_TOKEN`: {A_GITLAB_PERSONAL_ACCESS_TOKEN_WITH_API_SCOPE}



